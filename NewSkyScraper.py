from time import time, sleep
from datetime import datetime, timedelta
import datetime
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
import pandas as pd
from pandas import ExcelWriter
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

start_time = time()

def date_splitter(dateToSplit):
    dateToSplit = dateToSplit.split('-', 1)
    year = dateToSplit[0]
    dateToSplit = dateToSplit[1].split('-', 1)
    month = dateToSplit[0]
    day = dateToSplit[1]
    return year, month, day

def airline_extractor(sampleStr):
    x = sampleStr.split('overflow">', 1)
    x = x[1].split('</span', 1)
    airlineName = x[0]
    return airlineName

def price_extractor(otherStr):
    priceVal = otherStr.replace(',', '')
    priceVal = priceVal.replace(' Rs.', '')
    priceVal = round(float(priceVal) / 80, 2)
    return priceVal

def soup_extractor(scrapeLink):
    profile = webdriver.FirefoxProfile()
    ua1 = UserAgent()
    profile.set_preference('general.useragent.override', str(ua1.random))
    driver = webdriver.Firefox(profile)
    driver.get(scrapeLink)
    delay = 3000
    while True:
        try:
            WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.CLASS_NAME, 'accordion-content')))
            print('Page is ready!')
            break 
        except TimeoutException:
            print('Loading took too much time!')
    
    html = driver.execute_script('return document.body.innerHTML')
    driver.close() 

    b_html = BeautifulSoup(html,'html.parser') 
    targetSoup = b_html.find_all('div', class_ = 'accordion-content')[7]
    
    aList = []
    pList = []
    
    air = targetSoup.find_all('span', class_='clip-overflow')
    for i in range(len(air)):
        aList.append(targetSoup.find_all('span', class_='clip-overflow')[i].get_text())
        
    rs = targetSoup.find_all('span', class_='filter-price')
    for i in range(len(rs)):
        pList.append(price_extractor(targetSoup.find_all('span', class_='filter-price')[i].get_text()))

    newDf1 = pd.DataFrame()
    newDf1['airline'] = aList
    newDf1['price'] = pList
    newDf1['scraperDate'] = str(datetime.date.today())
    newDf1['departDate'] = d_date
    newDf1['returnDate'] = r_date
    newDf1['dayInAdvance'] = dayInAdvance
    newDf1['departMonth'] = d_month
    newDf1['returnMonth'] = r_month
    newDf1['departMonth'] = newDf1['departMonth'].map({'01': 'January', '02': 'February', '03': 'March', 
                            '04': 'April', '05': 'May', '06': 'June', '07': 'July', '08': 'August', 
                            '09': 'September', '10': 'October', '11': 'November', '12': 'December'})
    newDf1['returnMonth'] = newDf1['returnMonth'].map({'01': 'January', '02': 'February', '03': 'March', 
                            '04': 'April', '05': 'May', '06': 'June', '07': 'July', '08': 'August', 
                            '09': 'September', '10': 'October', '11': 'November', '12': 'December'})
    if(len(newDf1) > 1):
        x = newDf1.copy()
    else:
        x = soup_extractor(scrapeLink)
    print(x)
    return x

xlsDf = pd.DataFrame()

for i in range(1, 8):
    if(i == 1):
        dayInAdvance = 1
        d_date = str(datetime.date.today() + timedelta(days = 1))
        d_year, d_month, d_day = date_splitter(d_date)
        r_date = str(datetime.date.today() + timedelta(days = 31))
        r_year, r_month, r_day = date_splitter(r_date)
        scrapeLink = 'https://flight.yatra.com/air-search-ui/int2/trigger?type=R&viewName=normal&flexi=0&noOfSegments=2&origin=MUC&originCountry=DE&destination=MAA&destinationCountry=IN&flight_depart_date=' + d_day + '/' + d_month + '/' + d_year + '&arrivalDate=' + r_day + '/' + r_month + '/' + r_year + '&ADT=1&CHD=0&INF=0&class=Economy&source=fresco-home&version=1.1'
        print('Scraping data from', scrapeLink)
    elif(i == 2):
        dayInAdvance = 3
        d_date = str(datetime.date.today() + timedelta(days = 3))
        d_year, d_month, d_day = date_splitter(d_date)
        r_date = str(datetime.date.today() + timedelta(days = 33))
        r_year, r_month, r_day = date_splitter(r_date)
        scrapeLink = 'https://flight.yatra.com/air-search-ui/int2/trigger?type=R&viewName=normal&flexi=0&noOfSegments=2&origin=MUC&originCountry=DE&destination=MAA&destinationCountry=IN&flight_depart_date=' + d_day + '/' + d_month + '/' + d_year + '&arrivalDate=' + r_day + '/' + r_month + '/' + r_year + '&ADT=1&CHD=0&INF=0&class=Economy&source=fresco-home&version=1.1'
        print('Scraping data from', scrapeLink)
    elif(i == 3):
        dayInAdvance = 7
        d_date = str(datetime.date.today() + timedelta(days = 7))
        d_year, d_month, d_day = date_splitter(d_date)
        r_date = str(datetime.date.today() + timedelta(days = 37))
        r_year, r_month, r_day = date_splitter(r_date)
        scrapeLink = 'https://flight.yatra.com/air-search-ui/int2/trigger?type=R&viewName=normal&flexi=0&noOfSegments=2&origin=MUC&originCountry=DE&destination=MAA&destinationCountry=IN&flight_depart_date=' + d_day + '/' + d_month + '/' + d_year + '&arrivalDate=' + r_day + '/' + r_month + '/' + r_year + '&ADT=1&CHD=0&INF=0&class=Economy&source=fresco-home&version=1.1'
        print('Scraping data from', scrapeLink)
    elif(i == 4):
        dayInAdvance = 15
        d_date = str(datetime.date.today() + timedelta(days = 15))
        d_year, d_month, d_day = date_splitter(d_date)
        r_date = str(datetime.date.today() + timedelta(days = 45))
        r_year, r_month, r_day = date_splitter(r_date)
        scrapeLink = 'https://flight.yatra.com/air-search-ui/int2/trigger?type=R&viewName=normal&flexi=0&noOfSegments=2&origin=MUC&originCountry=DE&destination=MAA&destinationCountry=IN&flight_depart_date=' + d_day + '/' + d_month + '/' + d_year + '&arrivalDate=' + r_day + '/' + r_month + '/' + r_year + '&ADT=1&CHD=0&INF=0&class=Economy&source=fresco-home&version=1.1'
        print('Scraping data from', scrapeLink)
    elif(i == 5):
        dayInAdvance = 30
        d_date = str(datetime.date.today() + timedelta(days = 30))
        d_year, d_month, d_day = date_splitter(d_date)
        r_date = str(datetime.date.today() + timedelta(days = 60))
        r_year, r_month, r_day = date_splitter(r_date)
        scrapeLink = 'https://flight.yatra.com/air-search-ui/int2/trigger?type=R&viewName=normal&flexi=0&noOfSegments=2&origin=MUC&originCountry=DE&destination=MAA&destinationCountry=IN&flight_depart_date=' + d_day + '/' + d_month + '/' + d_year + '&arrivalDate=' + r_day + '/' + r_month + '/' + r_year + '&ADT=1&CHD=0&INF=0&class=Economy&source=fresco-home&version=1.1'
        print('Scraping data from', scrapeLink)
    elif(i == 6):
        dayInAdvance = 60
        d_date = str(datetime.date.today() + timedelta(days = 60))
        d_year, d_month, d_day = date_splitter(d_date)
        r_date = str(datetime.date.today() + timedelta(days = 90))
        r_year, r_month, r_day = date_splitter(r_date)
        scrapeLink = 'https://flight.yatra.com/air-search-ui/int2/trigger?type=R&viewName=normal&flexi=0&noOfSegments=2&origin=MUC&originCountry=DE&destination=MAA&destinationCountry=IN&flight_depart_date=' + d_day + '/' + d_month + '/' + d_year + '&arrivalDate=' + r_day + '/' + r_month + '/' + r_year + '&ADT=1&CHD=0&INF=0&class=Economy&source=fresco-home&version=1.1'
        print('Scraping data from', scrapeLink)
    elif(i == 7):
        dayInAdvance = 90
        d_date = str(datetime.date.today() + timedelta(days = 90))
        d_year, d_month, d_day = date_splitter(d_date)
        r_date = str(datetime.date.today() + timedelta(days = 120))
        r_year, r_month, r_day = date_splitter(r_date)
        scrapeLink = 'https://flight.yatra.com/air-search-ui/int2/trigger?type=R&viewName=normal&flexi=0&noOfSegments=2&origin=MUC&originCountry=DE&destination=MAA&destinationCountry=IN&flight_depart_date=' + d_day + '/' + d_month + '/' + d_year + '&arrivalDate=' + r_day + '/' + r_month + '/' + r_year + '&ADT=1&CHD=0&INF=0&class=Economy&source=fresco-home&version=1.1'
        print('Scraping data from', scrapeLink)

    """
    profile = webdriver.FirefoxProfile()
    ua1 = UserAgent()
    profile.set_preference('general.useragent.override', str(ua1.random))
    driver = webdriver.Firefox(profile)
    driver.get(scrapeLink)
    delay = 3000
    while True:
        try:
            WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.CLASS_NAME, 'accordion-content')))
            print('Page is ready!')
            break 
        except TimeoutException:
            print('Loading took too much time!')
    
    html = driver.execute_script('return document.body.innerHTML')
    driver.close() 

    b_html = BeautifulSoup(html,'html.parser') 
    targetSoup = b_html.find_all('div', class_ = 'accordion-content')[7]
    
    aList = []
    pList = []
    
    air = targetSoup.find_all('span', class_='clip-overflow')
    for i in range(len(air)):
        aList.append(targetSoup.find_all('span', class_='clip-overflow')[i].get_text())
        
    rs = targetSoup.find_all('span', class_='filter-price')
    for i in range(len(rs)):
        pList.append(price_extractor(targetSoup.find_all('span', class_='filter-price')[i].get_text()))

    newDf = pd.DataFrame()
    newDf['airline'] = aList
    newDf['price'] = pList
    newDf['scraperDate'] = str(datetime.date.today())
    newDf['departDate'] = d_date
    newDf['returnDate'] = r_date
    newDf['dayInAdvance'] = dayInAdvance
    newDf['departMonth'] = d_month
    newDf['returnMonth'] = r_month
    newDf['departMonth'] = newDf['departMonth'].map({'01': 'January', '02': 'February', '03': 'March', 
                            '04': 'April', '05': 'May', '06': 'June', '07': 'July', '08': 'August', 
                            '09': 'September', '10': 'October', '11': 'November', '12': 'December'})
    newDf['returnMonth'] = newDf['returnMonth'].map({'01': 'January', '02': 'February', '03': 'March', 
                            '04': 'April', '05': 'May', '06': 'June', '07': 'July', '08': 'August', 
                            '09': 'September', '10': 'October', '11': 'November', '12': 'December'})
    print(newDf)
    """
    newDf = soup_extractor(scrapeLink)
    xlsDf = xlsDf.append(newDf, ignore_index = True)
    sleep(100)

c_date = str(datetime.date.today())
c_year, c_month, c_day = date_splitter(c_date)

file_name = 'data/_' + c_year + c_month + c_day + '_muc_maa_sky.xlsx'
writer = pd.ExcelWriter(file_name, engine = 'xlsxwriter')
xlsDf.to_excel(writer, index = False, sheet_name = 'Sheet1')
writer.save()

end_time = time()

total_time = round(end_time - start_time, 2)

print('Execution time was', total_time, 'seconds')