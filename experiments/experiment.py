from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup as bs
from fake_useragent import UserAgent

url = 'https://www.skyscanner.net/transport/flights/muc/maa/180630/180730/?adults=1&children=0&adultsv2=1&childrenv2=&infants=0&cabinclass=economy&rtn=1&preferdirects=false&outboundaltsenabled=false&inboundaltsenabled=false&ref=home#results'
"""
driver = webdriver.Firefox()
driver.get(url)
html = driver.execute_script("return document.body.innerHTML")
driver.close() 
"""

profile = webdriver.FirefoxProfile()
ua1 = UserAgent()
profile.set_preference('general.useragent.override', str(ua1.random))
driver = webdriver.Firefox(profile)
driver.get(url)
delay = 60
while True:
    try:
        WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.CLASS_NAME, 'airlines')))
        print('Page is ready!')
        break 
    except TimeoutException:
        print('Loading took too much time!')
html = driver.execute_script('return document.body.innerHTML')
driver.close()

b_html = bs(html,'html.parser') 
x = b_html.find_all('ol', class_='airlines')

aList = []
pList = []

for row in x:
    y = row.find_all('span', class_ = 'label-text')
    z = row.find_all('span', class_ = 'filter-sub')

    for i in range(len(y)):
        aList.append(y[i])
        pList.append(z[i])
        
for k in range(len(aList)):
    print(aList[k], 'available at', pList[k])





#!/usr/bin/python3
# -*- coding: utf8 -*-
import requests
from bs4 import BeautifulSoup

__author__ = "Madam Pu"
url = "https://www.expedia.co.in/Flights-Search?flight-type=on&starDate=23%2F01%2F2019&endDate=30%2F01%2F2019&mode=search&trip=roundtrip&leg1=from%3AAtlanta%2C+GA%2C+United+States+%28ATL-All+Airports%29%2Cto%3AChennai%2C+India+%28MAA-Chennai+Intl.%29%2Cdeparture%3A23%2F01%2F2019TANYT&leg2=from%3AChennai%2C+India+%28MAA-Chennai+Intl.%29%2Cto%3AAtlanta%2C+GA%2C+United+States+%28ATL-All+Airports%29%2Cdeparture%3A30%2F01%2F2019TANYT&passengers=children%3A0%2Cadults%3A1%2Cseniors%3A0%2Cinfantinlap%3AY"

req = requests.get(url)
html = BeautifulSoup(req.text, "html.parser")
content_tag = html.find("fieldset", attrs={"id": "airlines",
                                      "class": "filter-set"})
print(content_tag)



####################



#!/usr/bin/python3
# -*- coding: utf8 -*-
from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By

__author__ = "Engine Bai"
url = "https://www.expedia.co.in/Flights-Search?flight-type=on&starDate=23%2F01%2F2019&endDate=30%2F01%2F2019&mode=search&trip=roundtrip&leg1=from%3AAtlanta%2C+GA%2C+United+States+%28ATL-All+Airports%29%2Cto%3AChennai%2C+India+%28MAA-Chennai+Intl.%29%2Cdeparture%3A23%2F01%2F2019TANYT&leg2=from%3AChennai%2C+India+%28MAA-Chennai+Intl.%29%2Cto%3AAtlanta%2C+GA%2C+United+States+%28ATL-All+Airports%29%2Cdeparture%3A30%2F01%2F2019TANYT&passengers=children%3A0%2Cadults%3A1%2Cseniors%3A0%2Cinfantinlap%3AY"

driver = webdriver.Firefox()
driver.get(url)
content_element = driver.find_element(By.ID, value="airlines")
content_html = content_element.get_attribute("innerHTML")

soup = BeautifulSoup(content_html, "html.parser")
p_tags = soup.find_all('span', class_ = "inline-label")
for p in range(10):
    print('\n')
    print(soup.find_all('span', class_ = "inline-label")[p].get_text())
    print(soup.find_all('div', class_ = "item-price")[p].get_text())
driver.close()